using Aplicacao.Interfaces.Genericos;
using Entidades;

namespace Aplicacao.Interfaces
{
    public interface IAplicacaoNoticia : IGenericaAplicacao<Noticia>
    {
         Task AdicionaNoticia(Noticia noticia);

         Task AtualizaNoticia(Noticia noticia);

         Task<List<Noticia>> ListarNoticiasAtivas();
    }
}