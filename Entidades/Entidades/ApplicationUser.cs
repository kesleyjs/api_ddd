using System.ComponentModel.DataAnnotations.Schema;
using Entidades.Enums;
using Microsoft.AspNetCore.Identity;

namespace Entidades
{
    public class ApplicationUser : IdentityUser
    {
        [Column("USR_IDADE")]
        public int Idade { get; set; }
        [Column("USR_Celular")]
        public string Celular { get; set; }
        [Column("USR_Tipo")]
        public TipoUsuario? Tipo { get; set; }
    }
}