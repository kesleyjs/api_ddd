using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Entidades.Notificacoes;

namespace Entidades
{
    // Define o nome da tabelo com a linha abaixo
    [Table("TB_NOTICIA")]
    public class Noticia : Notifica
    {
        // Define o nome da propriedade da tabela
        [Column("NTC_ID")]
        public int Id { get; set; }

        [Column("NTC_TITULO")]
        [MaxLength(255)]
        public string Titulo { get; set; }

        [Column("NTC_INFORMACAO")]
        [MaxLength(255)]
        public string Informacao { get; set; }

        [Column("NTC_ATIVO")]
        public bool Ativo { get; set; }

        [Column("NTC_DATA_CADASTRO")]
        public DateTime DataCadastro { get; set; }
        
        [Column("NTC_DATA_ALTERACAO")]
        public DateTime DataAlteracao { get; set; }

        // FK de ApplicatinUser
        [ForeignKey("ApplicationUser")]
        [Column(Order = 1)]
        public string UserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}