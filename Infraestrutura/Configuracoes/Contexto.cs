using Entidades;
using Infraestrutura.Mapeamento;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infraestrutura.Configuracoes
{
    public class Contexto : IdentityDbContext<ApplicationUser>
    {
        public Contexto(DbContextOptions<Contexto> opcoes) : base(opcoes)
        {
        }

        public DbSet<Noticia> Noticia { get; set; }
        public DbSet<ApplicationUser> ApplicationUser { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=localhost;port=3306;database=api_ddd;user=root;password=Bh312258*",
                new MySqlServerVersion(new Version(8, 0, 11)));
                // base.OnConfiguring(optionsBuilder);
            }
        }

        // Sobrescrevendo o método do IdentityDbContext
        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Definindo PK da tabela AspNetUsers
            builder.Entity<ApplicationUser>().ToTable("AspNetUsers").HasKey(t => t.Id);

            builder.Entity<Noticia>(new NoticiaMap().Configure);
            
            base.OnModelCreating(builder);
        }

        // public string ObterStringConexao()
        // {
        //     string strCon = "server=localhost; port=3306; database=API_DDD; user=root; password=Bh312258*";

        //     return strCon;
        // }
    }
}