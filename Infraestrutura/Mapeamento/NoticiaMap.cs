using Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestrutura.Mapeamento
{
    public class NoticiaMap : IEntityTypeConfiguration<Noticia>
    {
        public void Configure(EntityTypeBuilder<Noticia> builder)
        {
            builder.ToTable("Noticia");

            builder.HasKey(prop => prop.Id);

            builder.Property(prop => prop.Titulo)
                .HasConversion(prop => prop.ToString(), prop => prop)
                .IsRequired()
                .HasColumnName("Titulo")
                .HasColumnType("varchar(255)");
            
            builder.Property(prop => prop.Informacao)
                .HasConversion(prop => prop.ToString(), prop => prop)
                .IsRequired()
                .HasColumnName("Informacao")
                .HasColumnType("varchar(255)");
            
            builder.Property(prop => prop.Informacao)
                .HasConversion(prop => prop.ToString(), prop => prop)
                .IsRequired()
                .HasColumnName("Informacao")
                .HasColumnType("varchar(255)");

            builder.Property<DateTime>(prop => prop.DataCadastro)
                .HasColumnName("DataCadastro")
                .HasColumnType("DATETIME")
                .IsRequired();
            
            builder.Property<DateTime>(prop => prop.DataAlteracao)
                .HasColumnName("DataAlteracao")
                .HasColumnType("DATETIME")
                .IsRequired();
        }
    }
}